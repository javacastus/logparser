package com.ef;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import com.ef.config.AppConfig;
import com.ef.parser.InsertLogLoader;

@SpringBootApplication
@Import(AppConfig.class)
public class Parser {

	public static void main(String[] args) {
		String path=args[0];
		String startDate=args[1];
		String duration=args[2];
		String threshold=args[3];
		ApplicationContext ctx=null;
		InsertLogLoader loader=null;
		//create container
		ctx=SpringApplication.run(Parser.class, args);
		//get bean
		loader=ctx.getBean("loader",InsertLogLoader.class);
		//invoke method
		loader.loader(path);
		loader.retrivedIp(startDate, duration, threshold);
		//close container
		((ConfigurableApplicationContext) ctx).close();
	}
}

