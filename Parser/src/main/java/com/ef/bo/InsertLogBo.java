package com.ef.bo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class InsertLogBo {
	 private String log_date;          
	 private String log_ipaddress;       
	 private String log_request;           
	 private String log_status;              
	 private String log_user;               
}
