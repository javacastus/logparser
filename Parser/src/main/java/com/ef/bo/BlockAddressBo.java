package com.ef.bo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BlockAddressBo{
	private String ip; 
	private String comments;
}
