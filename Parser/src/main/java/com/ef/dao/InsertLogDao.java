package com.ef.dao;

import java.util.List;

import com.ef.bo.BlockAddressBo;
import com.ef.bo.InsertLogBo;


public interface InsertLogDao   {	
	public int insertLog(InsertLogBo bo);
	public List<InsertLogBo> getIp(String date,String duration,String threshold);
	public int insertIP(BlockAddressBo bo);
}
