package com.ef.dao;

import java.util.ArrayList;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;



import com.ef.bo.BlockAddressBo;
import com.ef.bo.InsertLogBo;
import com.ef.utils.ParserQuery;import com.mysql.jdbc.ResultSetMetaData;


@Repository
public class InsertLogDaoImpl implements InsertLogDao {
	
	@Autowired
	private JdbcTemplate jt;

	@Override
	public int insertLog(InsertLogBo bo) {
		int count=0;
		count=jt.update(ParserQuery.INSERT_LOG,bo.getLog_status(),bo.getLog_ipaddress(),bo.getLog_request().replaceAll("\"", "").trim(),bo.getLog_user().replaceAll("\"", "").trim(),bo.getLog_date());
		return count;
	}

	@Override
	public List<InsertLogBo> getIp(String date,String duration,String threshold) {
		List<InsertLogBo> listbo1=null;
		listbo1=jt.query(ParserQuery.SQL_RETRIEVEIP,rs->{
			java.sql.ResultSetMetaData rsmd=null;
			List<InsertLogBo> listbo = null;
			int colCount=0;
			listbo = new ArrayList<>();
			rsmd=rs.getMetaData();
			colCount=rsmd.getColumnCount();
			for(int i=1;i<=colCount;i++){
				System.out.println(rsmd.getColumnName(i).toUpperCase());
			}
			while (rs.next()) {
				InsertLogBo bo1 = new InsertLogBo();
				bo1.setLog_ipaddress(rs.getString(1));
				listbo.add(bo1);
			}// while
			return listbo;
		},date,date,threshold);
		return listbo1;
	}

	@Override
	public int insertIP(BlockAddressBo bo) {
		int count=0;
		count=jt.update(ParserQuery.INSERT_IP,bo.getIp(),bo.getComments());
		return count;
	}
}
