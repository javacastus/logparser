package com.ef.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"com.ef.dao,com.ef.service,com.ef.loader"})
public class AppConfig {

}
