package com.ef.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class InsertLogDto implements Serializable {
	 private String log_date;          
	 private String log_ipaddress;       
	 private String log_request;           
	 private String log_status;              
	 private String log_user;               
}
