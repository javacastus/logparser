package com.ef.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BlockAddressDto implements Serializable{
	private String ip; 
	private String comments;
}
