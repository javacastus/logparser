package com.ef.utils;

public final class ParserQuery {
	public static final String INSERT_LOG="INSERT INTO LOG_ROW VALUES(ID,?,?,?,?,?)";
	public static final String SQL_RETRIEVEIP = "SELECT  IP  FROM   LOG_ROW  WHERE STARTDATE BETWEEN CAST(?  AS DATETIME) AND CAST(? AS DATETIME) + INTERVAL 1 HOUR GROUP BY IP HAVING COUNT(IP) >=? ";
	public static final String INSERT_IP="INSERT INTO BLOCKED_ADDRESS VALUES(ID,?,?)";
}
