package com.ef.service;

import java.util.List;

import com.ef.dto.BlockAddressDto;
import com.ef.dto.InsertLogDto;

public interface InsertLogService {
	public int parser(InsertLogDto dto);
	public List<InsertLogDto> ipRetrival(String date,String duration,String threshold);
	public int insertIp(BlockAddressDto dto);
}
