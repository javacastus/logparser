package com.ef.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ef.bo.BlockAddressBo;
import com.ef.bo.InsertLogBo;
import com.ef.dao.InsertLogDao;
import com.ef.dto.BlockAddressDto;
import com.ef.dto.InsertLogDto;

@Service
public class InsertLogServiceImpl implements InsertLogService {

	@Autowired
	private InsertLogDao dao;

	@Override
	public int parser(InsertLogDto dto) {
		int count = 0;
		InsertLogBo bo = null;
		// convert dto to bo
		bo = new InsertLogBo();
		BeanUtils.copyProperties(dto, bo);
		count = dao.insertLog(bo);
		return count;
	}

	@Override
	public List<InsertLogDto> ipRetrival(String date, String duration,
			String threshold) {
		List<InsertLogBo> listbo = null;
		List<InsertLogDto> listdto = new ArrayList<InsertLogDto>();
		//use service
		listbo = dao.getIp(date, duration, threshold);
		listbo.forEach(bo -> {
			InsertLogDto dto = new InsertLogDto();
			dto.setLog_ipaddress(bo.getLog_ipaddress());
			listdto.add(dto);
		});
		return listdto;
	}

	@Override
	public int insertIp(BlockAddressDto dto) {
		int count=0;
		BlockAddressBo bo=null;
		// convert dto to bo
		bo=new BlockAddressBo();
		BeanUtils.copyProperties(dto, bo);
		//use service
		count=dao.insertIP(bo);
		return count;
	}
	
}