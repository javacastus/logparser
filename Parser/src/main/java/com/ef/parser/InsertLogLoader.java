package com.ef.parser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ef.dto.BlockAddressDto;
import com.ef.dto.InsertLogDto;
import com.ef.service.InsertLogService;

@Component("loader")
public class InsertLogLoader {

	@Autowired
	private InsertLogService service;

	public void loader(String path) {
		String line = null;
		InsertLogDto dto = null;
		String[] var = null;
		try (FileInputStream inputStream = new FileInputStream(path);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						inputStream))) {
			while ((line = in.readLine())!= null) {
				dto = new InsertLogDto();
				var = line.split(Pattern.quote("|"));
				dto.setLog_status(var[3]);
				dto.setLog_ipaddress(var[1]);
				dto.setLog_request(var[2]);
				dto.setLog_user(var[4]);
				dto.setLog_date(var[0]);
				 service.parser(dto);
			}
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public void retrivedIp(String startDate, String duration, String threshold) {
		List<InsertLogDto> listdto = null;
		// invoke method
		listdto = service.ipRetrival(startDate, duration, threshold);
		//inserting ip and printing in console
		listdto.forEach(dto -> {
			BlockAddressDto bdto=null;
			System.out.println(dto.getLog_ipaddress());
			//copy properties dto to bdto
			bdto=new BlockAddressDto();
			bdto.setIp(dto.getLog_ipaddress());
			bdto.setComments("Blocked IP");
			//invoke method
			service.insertIp(bdto);
		});
	}
}